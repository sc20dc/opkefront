import './App.css';

import {BrowserRouter} from "react-router-dom";
import {Route, Routes} from "react-router";
import PokemonStart from "./Components/PokemonStart";
import SelectPokemon from "./Components/SelectPokemon";
import Fight from "./Components/Fight";


function App() {
  return (


      <div className="App">
          <BrowserRouter>
              <Routes>

                  <Route path="/" exact element={<PokemonStart/>} />
                  <Route path="/select" exact element={<SelectPokemon/>} />
                  <Route path="/fight" exact element={<Fight/>} />

              </Routes>
          </BrowserRouter>
      </div>

  );
}



export default App;
