import React, {Fragment, useEffect, useState} from "react";
import {useLocation, useNavigate} from "react-router";
import {Button} from "@mui/material";
import ReplyIcon from '@mui/icons-material/Reply';
import SportsKabaddiIcon from '@mui/icons-material/SportsKabaddi';
import SportsMartialArtsIcon from '@mui/icons-material/SportsMartialArts';
import "./CSS/FlashingImages.css"
import "./CSS/PokeNames.css"
import axios from "axios";




function Fight(){

    const link = useLocation();
    const [pokemon, setPokemon] = useState();
    const navigateBack = useNavigate();
    const [pokeWinnerOne, setPokeWinnerOne] = useState(null)
    const [pokeWinnerTwo, setPokeWinnerTwo] = useState(null)
    const [one, setOne] = useState()
    const [two, setTwo] = useState()
    const [myStats, setMyStats] = useState({"pokemonName": "",
                                                    "hp": 0, "speed": 0, "defence": 0, "attack": 0})
    const [enemyStats, setEnemyStats] = useState({"pokemonName": "",
        "hp": 0, "speed": 0, "defence": 0, "attack": 0})


    function goBack(){
        navigateBack("/select")
        setPokeWinnerOne(null)
        setPokeWinnerTwo(null)
    }


    function getWinner(e){
            console.log("MyStats : " + myStats)
            axios.get("http://localhost:8080/getAWinner?my="+one+"&enemy=" + two).then(e => {
                if (e.data === one) {
                    console.log("one" +  e.data)
                    setPokeWinnerOne(e.data)

                } else {
                    console.log("two" +  e.data)
                    setPokeWinnerTwo(e.data)
                }
            })
    }


    useEffect(() => {
        setPokemon(link.state.linkName)
    }, [link]);

    useEffect(() => {
        axios.get("http://localhost:8080/getPokemonByname?name=" + one).then(e => setMyStats(e.data))
        axios.get("http://localhost:8080/getPokemonByname?name=" + two).then(e => setEnemyStats(e.data))
    }, [one, two]);

    useEffect(() => {
        setOne(link.state.name)
    }, []);

    useEffect(() => {
        axios.get("http://10.41.154.163:5000/api/"+one).then(e => setTwo(e.data))
        console.log("HERE")
    }, [one]);

    return(


        <Fragment>

            <div style={{display: "inline-flex", marginTop: "6%"}}>
                {pokeWinnerOne ? <img style={{height: "190px", maxWidth: "290px", marginTop: "25%", zIndex: "2", position: "absolute", marginLeft: "120px"}} src={"/win.png"} alt="Pokemon" /> : null}
                {pokeWinnerTwo ? <img style={{height: "190px", maxWidth: "290px", marginTop: "25%", zIndex: "2", position: "absolute", marginLeft: "1120px"}} src={"/win.png"} alt="Pokemon" /> : null}
                {pokeWinnerOne ? <div id={"pokeNames2"} style={{height: "190px", maxWidth: "290px", marginTop: "5%", zIndex: "2", position: "absolute", marginLeft: "660px"}}>You Win!</div> : null}
                {pokeWinnerTwo ? <div id={"pokeNames2"} style={{height: "190px", maxWidth: "290px", marginTop: "5%", zIndex: "2", position: "absolute", marginLeft: "660px"}}>You Lose!</div> : null}


                <div id={"pikaFight"}> <img style={{width: "450px"}} src={pokemon} />
                    <span style={{display: "block"}}>
                        <p id={"textOne"}>STATS: </p>
                        <p id={"textOne"}>NAME: {myStats["pokemonName"]}  </p>
                        <p id={"textOne"}>HP: {myStats["hp"]} </p>
                        <p id={"textOne"}>SPEED: {myStats["speed"]} </p>
                        <p id={"textOne"}>ATTACK: {myStats["attack"]} </p>
                        <p id={"textOne"}>DEFENCE: {myStats["defence"]}  </p>
                    </span>
                </div>

                   <Button onClick={e => getWinner(e)} id={"fightButton"} startIcon={<SportsMartialArtsIcon/>} endIcon={<SportsKabaddiIcon/>} >Fight</Button>

                <div id={"pikaFight2"}><img style={{width: "450px"}} src={"/imgPokemon/"+two+".png"} />
                    <span style={{display: "block"}}>
                        <p id={"textOne"}>STATS: </p>
                        <p id={"textOne"}>NAME: {enemyStats["pokemonName"]}  </p>
                        <p id={"textOne"}>HP: {enemyStats["hp"]} </p>
                        <p id={"textOne"}>SPEED: {enemyStats["speed"]} </p>
                        <p id={"textOne"}>ATTACK: {enemyStats["attack"]} </p>
                        <p id={"textOne"}>DEFENCE: {enemyStats["defence"]}  </p>
                    </span>
                </div>

                <div style={{marginTop: "650px", zIndex: "2", position: "absolute", marginLeft: "660px", width: "200px", height: "50px"}}>
                    <Button style={{backgroundColor: "black", color: "white" ,}} variant="contained" onClick={e => goBack(e)} endIcon={<ReplyIcon />}>
                        <p style={{color: "white", fontWeight: "bold"}}>Go Back</p>
                    </Button>
                </div>
            </div>





        </Fragment>

    )
}

export default Fight;