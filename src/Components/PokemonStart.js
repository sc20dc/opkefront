import React, {Fragment} from "react";
import {Button} from "@mui/material";
import SendIcon from '@mui/icons-material/Send';
import {useNavigate} from "react-router";


function PokemonStart(){
    const navigate = useNavigate();

    function startGame(event){
        navigate("/select");
    }


    return(
        <Fragment>
            <div>
                <div>
                    <img style={{height: "auto", maxWidth: "100%", marginTop: "5%"}} src={"/pokemon-logo-png-1428.png"} alt="Pokemon" />
                </div>

                <img style={{maxWidth: "150px", maxHeight: "150px", marginLeft: 'calc(50%  1000px)', marginTop: "5%"}} src={"imgPokemon/start.png"}  alt={"no image"}/>
                <div>
                    <Button style={{background: "blue", color: "red", marginTop: "2%", width: "20%", height: "50px"}} variant="contained" endIcon={<SendIcon />} onClick={e => startGame(e)}>
                        <p style={{color: "white", fontWeight: "bold"}}>Start Game</p>
                    </Button>
                </div>
            </div>

        </Fragment>
    )
}

export default PokemonStart;