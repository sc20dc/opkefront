import React, {Fragment, useEffect, useState} from "react";
import "./CSS/FlashingImages.css"
import "./CSS/PokeNames.css"
import {Button} from "@mui/material";
import {useNavigate} from "react-router";
import axios from "axios";
import ReplyIcon from "@mui/icons-material/Reply";

function SelectPokemon(){

    const [pokemonNames, setPokemonNames] = useState([]);
    const [pokemonObjects, setPokemonObjects] = useState([]);

    const navigateBack = useNavigate();



    useEffect(() => {
        axios.get("http://localhost:8080/getAllPokemons").then(e => setPokemonObjects(e.data))
    }, []);


    useEffect(() => {
        let newList = []
       pokemonObjects.forEach((e) => {
           newList.push(e["pokemonName"])
       })
        setPokemonNames(newList)
    }, [pokemonObjects]);


        // useEffect(() => {
        //     pokemonObjects.forEach((e) => console.log(pokemonNames))
        // }, [pokemonNames]);
        //
        //








    function goBack(){
        navigateBack("/")
    }

    function goFight(e, item){
        console.log(e.target.src)
        let link = e.target.src

        navigateBack("/fight", {state: {"linkName" : link, "name": item}})
    }

    let durationBody = pokemonNames.map((item, i) => {
        return (

                <div   style={{maxWidth : "400px", flexWrap: "nowrap", display: "inline-block", padding: "1%"}} key={i}>

                    <img onClick={e => goFight(e, item) } style={{width: "260px", height: "320px",  backgroundColor: "yellow" }} src={"imgPokemon/"+item+".png"}  alt={"no Image"}/>
                    <div id={"pokeNames"}>{item}</div>

                </div>

        );
    });




    return(
        <Fragment>

                <h1 id={"pokeNames2"} >Select Pokemon</h1>



            <div className={"hover13"}>
                    {durationBody}
            </div>

            <Button style={{background: "black", color: "white", marginTop: "2%", width: "10%", height: "50px", marginBottom: "20px"}} variant="contained" onClick={e => goBack(e)} endIcon={<ReplyIcon />}>
                <p style={{color: "white", fontWeight: "bold"}}>Go Back</p>
            </Button>

        </Fragment>
    )
}

export default SelectPokemon;